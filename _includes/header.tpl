<header>
	<h1>{% if page.title %}{{ page.title }}{% else %}{{ site.name }}{% endif %}</h1>
	{% if page.title == null %}<p class="additional">{{ site.desc }}</p>{% endif %}
</header>
