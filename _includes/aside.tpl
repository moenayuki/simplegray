<aside>
	<h2><a href="/">{{ site.name }}</a><!--<a href="/feed.xml" class="feed-link" title="Subscribe"><img src="http://blog.rexsong.com/wp-content/themes/rexsong/icon_feed.gif" alt="RSS feed" /></a>--></h2>
	
	<nav class="block">
		<ul>
		{% for category in site.custom.categories %}<li class="{{ category.name }}"><a href="/categories/{{ category.name }}/">{{ category.title }}</a></li>
		{% endfor %}
		</ul>
	</nav>
	
	<form action="/search/" class="block block-search">
		<h3>搜索</h3>
		<p><input type="search" name="q" placeholder="少女施工中………" disabled="disabled"/></p>
	</form>
	
	<div class="block block-about">
		<h3>About</h3>
		<figure>
			<img src="/assets/img/kodachi.png" width="150" height="150" class="nostyle" />
			<figcaption><strong>{{ site.meta.author.name }}</strong></figcaption>
		</figure>
		<p>不学无术，好吃懒做。</p>
	</div>
	
	<div class="block block-license">
		<p>除非声明，本站所有文字内容使用<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/deed.zh">CC-BY-SA 4.0</a>授权。</p>
	</div>

	<div class="block block-thank">
		<p>自豪地采用 <a href="https://github.com/mojombo/jekyll" target="_blank">jekyll</a> + <a href="https://github.com/mytharcher/SimpleGray" target="_blank">SimpleGray</a></p>
	</div>

	<div class="block">
		网站龟速建设中，请无视各种404。
	</div>

</aside>